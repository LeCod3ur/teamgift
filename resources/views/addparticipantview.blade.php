@extends('layouts.principal')

@section('content')

    <div class="container mt-3 pt-5 pb-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Information sur l'évenement</h4>
                    </div>
                    <div class="card-body">
                        @foreach ($eventDetail as $item)
                            <h5 class="card-title fw-bold" style="text-transform: Capitalize">{{ $item->eventTitle }}</h5>
                            <div class="container">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Organiser par</div>
                                            {{ $item->FullName }}
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Date de l'évenement</div>
                                            {{ $item->DateOfEvent }}
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <form action="{{ route('eventdetail') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="ID" value="{{ $ID }}" />
                                            <input type="submit" class="btn btn-primary" value="Voir l'évènement" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-1 pt-5 pb-4">
        <div class="row">
            <div class="col-12">
                <h2>Personne à inviter</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-7  justify-content-md-end">
                <table class="table table-striped table-hover">
                    <thead class="table-primary active">
                        <tr>
                            <th scope="col" style="width:65%">Nom Complet</th>
                            <th scope="col" style="width:35%">Inviter une personne</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (sizeof($participantNoInvite) == null)
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">Tout le monde est invité</div>
                        </div>
                    @else
                        @foreach ($participantNoInvite as $item)
                            <form action="{{ route('addparticipant') }}" method="post">
                                {{ csrf_field() }}
                                <tr>
                                    <input type="hidden" name="eventID" value="{{ $ID }}" />
                                    <td>{{ $item->FullName }}</td>
                                    <td>
                                        <input type="hidden" name="participantID" value="{{ $item->ID }}" />
                                        <input type="submit" class="btn btn-primary" value="Inviter" /> <br />
                                    </td>
                                </tr>
                            </form>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="col-1">
            </div>
            <div class="col-4">
                <ul class="list-group list-group-numbered">
                    <a href="#" class="list-group-item list-group-item-action list-group-item-primary active disabled">
                        <div class="fw-bold">Personne Invitée</div>
                    </a>
                    @if (sizeof($participantInvite) == null)
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">Aucun Invité</div>
                        </div>
                    @else
                        @foreach ($participantInvite as $item)
                            <li
                                class="list-group-item d-flex justify-content-between list-group-item-action align-items-start pb-3 pt-3">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">{{ $item->FullName }}</div>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <script>
        $("document").ready(function() {
            $("a.btn").css("font-weight", "700");
            $("input").css("font-weight", "800");
        })
    </script>

@endsection

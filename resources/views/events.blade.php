@extends('layouts.principal')

@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <div class="container mt-1 pt-5 pb-4 pl-4">
        <div class="row">
            <div class="col-12">
                @foreach ($eventDetail as $item)
                    <h2 style="text-transform:capitalize">{{ $item->eventTitle }}</h2>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action list-group-item-primary active disabled">
                        <div class="fw-bold">Image du cadeau</div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action disabled">
                        <img src="{{ $item->PhotoURL }}" class="img-fluid round" alt="img-Cadeau">
                    </a>
                </div>
            </div>
            <div class="col-5 justify-content-md-end">
                <ul class="list-group">
                    @foreach ($eventDetail as $item)
                        <a href="#" class="list-group-item list-group-item-action list-group-item-primary active disabled">
                            <div class="fw-bold">Détail de l'évènement</div>
                        </a>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto" style="text-transform:capitalize">
                                <div class="fw-bold">Nom de l'évènement</div>
                                {{ $item->eventTitle }}
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Organiser par</div>
                                {{ $item->FullName }}
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Date de l'évènement</div>
                                {{ $item->DateOfEvent }}
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Nom du cadeau</div>
                                {{ $item->giftTitle }}
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Prix du cadeau</div>
                                {{ $item->Price }} $
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-start pb-4">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Montant Collecté</div>
                                {{ $montantCollecte }}$
                            </div>


                            <form action="faireundon" method="post">
                                {{ csrf_field() }}
                                <div class="input-group col-md-12" style="padding-top: 10px">
                                    <input type="hidden" name="eventID" value="{{ $ID }}" />
                                    <input type="hidden" name="participantID" value="{{ $userID }}" />
                                    <input type="number" type="number" name="contribution" step="0.01" min="5.00"
                                        max="1000.00" class="form-control" placeholder="0.00">
                                    <span class="input-group-text">$</span>
                                    <input type="submit" value="Contribuer" class="input-group-text"></a>
                                </div>
                            </form>
                        </li>
                    @endforeach

                </ul>
            </div>
            <div class="col">
                <ul class="list-group">
                    <a href="#" class="list-group-item list-group-item-action list-group-item-primary active disabled">
                        <div class="fw-bold">Personne Participant</div>
                    </a>
                    @if (sizeof($participantAccepter) == null)
                        <li
                            class="list-group-item d-flex justify-content-between list-group-item-action align-items-start pb-3 pt-3">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">Il n'y a pas de participant</div>
                            </div>
                        </li>
                    @else
                </ul>
                <ul class="list-group list-group-numbered">
                    @foreach ($participantAccepter as $item)
                        <li
                            class="list-group-item d-flex justify-content-between list-group-item-action align-items-start pb-3 pt-3">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold">{{ $item->FullName }}</div>
                            </div>
                        </li>
                    @endforeach
                    @endif

                </ul>
                <ul class="list-group pt-3">
                    <form action="{{ route('addparticipantview') }}" method="get">
                        {{ csrf_field() }}
                        <input type="hidden" name="ID" value="{{ $ID }}" />
                        <input type="submit" class="btn btn-secondary col-12" value="Inviter une personne" />
                    </form>
                </ul>
            </div>
        </div>
    </div>

    <script>
        $("document").ready(function() {
            $("#eventDetail").css("padding", "20px 0 40px 20px");
            $("input").css("font-weight", "800");
        })
    </script>
@endsection

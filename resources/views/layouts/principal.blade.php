<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TeamGift</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{ route('index') }}">
                    <img src="{{ asset('img\teamgiftLogo.png') }}" class="img-fluid" width="100px" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="nav justify-content-center">
                    <li class="nav-item pl-4 pr-4">
                        <a href="{{ route('index') }}" class="nav-link" aria-current="page" >Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('index') }}" class="nav-link">Mes Évènements</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="nav-link">Créer un évènement</a>
                    </li>
                </ul>
                <div class="d-flex">
                    <a href="{{ route('logout') }}" class="btn btn-outline-danger" stype="color:#dc3545" type="submit">Se deconnecter</a>
                </div>
            </div>
        </nav>
    </header>


    <main role="main" class="container">
        @yield('content')
    </main>

    <!-- <footer>
        <div class="footer js-dropdown">
            <div class="container">
                <div class="footer__nav">
                    <div class=" footer__bline">
                        <div class="footer__menu">
                            <div class="footer__copyright">
                                <span>&copy; 2021 <a href="{{ route('index') }}">TeamGift.inc</a> -
                                    Tous droits réservés.</span>
                            </div>
                        </div>
                        <div class="footer__social">
                            <div class="footer__copyright">
                                <p>Ce site a été fait avec <span>passion</span> et <span>beaucoup de nuits
                                        blanches</span>. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer> -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Créer l'évènement</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body col-12">
                    <form action="{{ route('eventcreateaction') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-4 col-form-label fw-700">Nom de l'évènement</label>
                            <div class="col-sm-8">
                                <input type="text" name="title" class="form-control form-control-lg" id="inputText">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputDate" class="col-sm-4 col-form-label fw-700">Date de l'évènement</label>
                            <div class="col-sm-8">
                                <input type="date" name="dateOfEvent" class="form-control form-control-lg" id="inputDate">
                            </div>
                        </div>
                        <input type="hidden" readonly name="OrganizerID" value="{{ $userID }}" /> <br />
                        <div class="container">
                            <fieldset class="row mb-3">
                                <legend class="col-form-label col-sm-4 pt-0 fw-800">Choisir le cadeau</legend>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Choisir</th>
                                            <th scope="col">Nom</th>
                                            <th scope="col">Prix</th>
                                            <th scope="col">Image</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($gifts as $item)
                                            <tr>
                                                <th scope="row">
                                                    <input class="form-check-input ml-3" type="radio" name="giftID" value="{{ $item->ID }}">
                                                </th>
                                                <td>{{ $item->Title }}</td>
                                                <td>{{ $item->Price }}</td>
                                                <td>
                                                    <img src="{{ $item->PhotoURL }}" class="img-fluid round" width="100px" />
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                    <input type="submit" class="btn btn-primary" value="Créer" />
                </div>
                </form>
            </div>
        </div>
    </div>




    <script>
        var myModal = document.getElementById('myModal')
        var myInput = document.getElementById('myInput')

        myModal.addEventListener('shown.bs.modal', function() {
            myInput.focus()
        })
    </script>

    <script>
        $("document").ready(function() {
            $("li.posts__item:even").addClass("bg-fef2e0");
            $("td").css("vertical-align", "middle");
            $("th").css("vertical-align", "middle");
            $("td").css("text-transform", "capitalize");
            $("a").css("font-weight", "700");

        })
    </script>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>

@extends('layouts.principal')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <div id="profilWelcome">
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="https://uploads-ssl.webflow.com/579cb3c627a52b781ae2b1ae/5ec6b5ce7f42aad6bfa001a5_memoji_v3.png"
                        class="img-fluid rounded-start" alt="avatar">
                </div>
                <div class="col-md-8">
                    <div class="card-header fw-bold">
                        <h5>Information sur l'utilisateur</h5>
                    </div>
                    <div class="card-body">
                        @foreach ($userActive as $item)
                            <h4 class="card-title fw-bold">Nom: {{ $item->FullName }}</h4>
                            <h6 class="card-text">Courriel: {{ $item->Email }}</h6>
                            <h6 class="card-text">ID: {{ $item->ID }}</h6>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="invitationWelcome">
        <h3 class="fw-900">Liste des invitations</h3>
        <br />
        <table class="table table-striped table-hover">
            <thead class="table-primary active">
                <tr>
                    <th scope="col">Nom de l'évènement</th>
                    <th scope="col">Date de l'évènement</th>
                    <th scope="col">Organisé par</th>
                    <th scope="col">Accepter/Refuser</th>
                    <th scope="col">Action </th>
                </tr>
            </thead>
            <tbody>
            @if (sizeof($invitation) == null)
                    <tr>
                        <td colspan="5" style="text-align: center; font-size:16pt; font-weight:700; font-style:italic">Aucune Invitation</td>
                    </tr>
                @else
                @foreach ($invitation as $invite)
                    <form action="{{ route('acceptinvitation') }}" method="post">
                        {{ csrf_field() }}
                        <tr>
                            <input type="hidden" name="eventID" value="{{ $invite->eventID }}" />
                            <input type="hidden" name="participantID" value="{{ $invite->ParticipantID }}" />
                            <td>{{ $invite->Title }}</td>
                            <td>{{ $invite->DateOfEvent }}</td>
                            <td>{{ $invite->FullName }}</td>
                            <td>
                                <div class="btn-group d-md-flex justify-content-md-end" name="accepted">
                                    <button type="submit" name="accepted" value="1"
                                        class="btn btn-outline-success">Accepter</button>
                                    <button type="submit" name="accepted" value="0"
                                        class="btn btn-outline-danger">Refuser</button>
                                </div>
                            </td>
                    </form>
                    <td>
                        <form action="{{ route('eventdetail') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="ID" value="{{ $invite->eventID }}" />
                            <input type="submit" class="btn btn-outline-primary" value="Voir Detail" />
                        </form>
                    </td>
                    </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>

    <div id="évènementWelcome">
        <h3 class="fw-900">Évènements à venir</h3>
        <br />
        <table class="table table-striped table-hover">
            <thead class="table-primary">
                <tr>
                    <th scope="col">Nom de l'évènement</th>
                    <th scope="col">Date de l'évènement</th>
                    <th scope="col">Organisé par</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if (sizeof($eventsOrg) == null && sizeof($eventsPart) == null)
                    <tr>
                        <td colspan="5" style="text-align: center; font-size:16pt; font-weight:700; font-style:italic">Aucun Évènement</td>
                    </tr>
                @else
                    @foreach ($eventsOrg as $item)
                        <tr>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->DateOfEvent }}</td>
                            <td>{{ $item->FullName }}</td>
                            <td>
                                <form action="{{ route('eventdetail') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="ID" value="{{ $item->eventID }}" />
                                    <input type="submit" class="btn btn-outline-primary" value="Voir Detail" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    @foreach ($eventsPart as $item)
                        <tr>
                            <td>{{ $item->Title }}</td>
                            <td>{{ $item->DateOfEvent }}</td>
                            <td>{{ $item->FullName }}</td>
                            <td>
                                <form action="{{ route('eventdetail') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="ID" value="{{ $item->eventID }}" />
                                    <input type="submit" class="btn btn-outline-primary" value="Voir Detail" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Créer un évènement
    </button>


    <script>
        $("document").ready(function() {
            $("button").css("font-weight", "700");
            $("a.btn").css("font-weight", "700");
            $("input").css("font-weight", "700");
            $("#profilWelcome").css("padding", "50px 0 20px 20px");
            $("#invitationWelcome").css("padding", "50px 0 20px 0");
            $("#évènementWelcome").css("padding", "50px 0 40px 0");

        })
    </script>
@endsection

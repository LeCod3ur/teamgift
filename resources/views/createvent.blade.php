@extends('layouts.principal')

@section('content')
    <form action="{{ route('eventcreateaction') }}" method="post">
        {{ csrf_field() }}
        <input type="text" name="title" placeholder="Titre" /> <br />
        <input type="text" name="dateOfEvent" placeholder="2021-12-25" /> <br />
        <input type="hidden" readonly name="OrganizerID" value="{{ $userID }}" /> <br />
        @foreach ($gifts as $item)
            <div>
                <input type="radio" name="giftID" value="{{ $item->ID }}" /> {{ $item->Title }}
                <p> {{ $item->Price }} </p>
                <img src="{{ $item->PhotoURL }}" height="auto" /> <br />
            </div>
        @endforeach

        <input type="submit" value="Soumettre" />
    </form>


    <br />



@endsection

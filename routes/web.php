<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\HelloController;
use App\Http\Controllers\IndexController;
use App\Models\Gift;
use Illuminate\Support\Facades\Route;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[IndexController::class,'index']);


// Bootstrap ne pas toucher
// Route::get('/welcome/{user}',[EventController::class,'index'])->name('hello');
// Auth::routes();

//Route Home
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/welcome', [App\Http\Controllers\HomeController::class, 'index'])->name('welcome');

//Route Event
//Route::get('/events',[EventController::class,'eventView'])->name('events');
Route::get('/events/create',[EventController::class, 'create'])->name('newevent');

//Route Connexion et Inscription
Route::get('/login',[IndexController::class, 'connection'])->name('login');
Route::get('/register',[IndexController::class, 'inscription'])->name('register');

Route::get('/logout',[IndexController::class, 'logout'])->name('logout');


Route::post('/loginaction', [IndexController::class, 'login'])->name('loginaction');
Route::post('/registeraction', [IndexController::class, 'register'])->name('registeraction');

Route::get('/welcome', [EventController::class, 'welcome'])->name('welcome');
Route::post('/acceptinvitation',[EventController::class, 'acceptInvitation'])->name('acceptinvitation');
Route::post('/invitationform',[EventController::class, 'invitationForm'])->name('invitationform');
Route::post('/eventdetail',[EventController::class, 'eventDetail'])->name('eventdetail');

Route::post('/faireundon', [EventController::class, 'faireUnDon'])->name('faireundon');

Route::get('/index', [IndexController::class,'index'])->name('index');
Route::get('/createeventview', [EventController::class, 'createEventView'])->name('createeventview');
Route::post('/eventcreateaction', [EventController::class, 'createEvent'])->name('eventcreateaction');

Route::get('/addparticipantview', [EventController::class, 'addParticipantView'])->name('addparticipantview');
Route::post('/addparticipant', [EventController::class, 'addParticipant'])->name('addparticipant');



//View Composer pour passer à plusieurs vue
View::composer('*', function ($view) {
    $userID = session()->get("userID");
    if ($userID != null){
        $userFullNameSession = session()->get("userFullname");
        $view->with('userFullNameSession', $userFullNameSession);
    }
    $gifts = Gift::all();
    if ($gifts != null){
        $view->with('gifts', $gifts);
    }
});

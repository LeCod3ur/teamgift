<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Session;
use App\Models\Gift;
use App\Models\Event;
use App\Models\Utilisateur;
use App\Models\Participation;
use Illuminate\Http\Request;
use PDO;

class IndexController extends Controller
{
    public function Index(Request $request)
    {

        $userID = session()->get("userID");
        if ($userID != null) {
            $userFullNameSession = session()->get("userFullname");
            $userActive = Utilisateur::all()->where('ID', $userID);
            $gifts = Gift::all();
            $eventsOrg = Event::join('User', 'User.ID', '=', 'Event.OrganizerID')->join('Gift', 'Gift.ID', '=', 'GiftID')->select('*', 'Gift.Title as giftTitle', 'Event.Title as title', 'Event.ID as eventID')->where('OrganizerID', $userID)->get();
            $eventsPart = Event::join('Participation', 'Participation.eventID', '=', 'Event.id')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.ID as eventID')->where('ParticipantID', $userID)->where('Accepted', 1)->get();
            $invitation = Participation::join('Event', 'Event.ID', '=', 'Participation.EventID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('Participation.EventID as eventID', 'ParticipantID', 'Title', 'DateOfEvent', 'FullName', 'OrganizerID')->where('Participation.ParticipantID', $userID)->where('Participation.Accepted', null)->distinct('[Participation.EventID]')->get();
            return view('welcome', compact('userFullNameSession', 'eventsPart', 'eventsOrg', 'invitation', 'userActive', 'gifts', 'userID'));
        } else
            return view("connexion.login");
    }

    public function connection()
    {
        return view('connexion.login');
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $user = Utilisateur::where('Email', $email)->where('Password', $password)->first();
        $userID = Utilisateur::where('Email', $email)->value('ID');
        $userFullName =  Utilisateur::where('Email', $email)->value('FullName');
        if ($user == null) {
            return view('connexion.login');
        } else {
            //ajouter un user connecter dans une variable de session
            $request->session()->put("userID", $userID);
            $request->session()->put("userFullname", $userFullName);

            $userIDSession = session()->get("userID");
            $userFullNameSession = session()->get("userFullname");
            $userActive = Utilisateur::all()->where('ID', $userID);
            $gifts = Gift::all();
            $eventsOrg = Event::join('User', 'User.ID', '=', 'Event.OrganizerID')->join('Gift', 'Gift.ID', '=', 'GiftID')->select('*', 'Gift.Title as giftTitle', 'Event.Title as title', 'Event.ID as eventID')->where('OrganizerID', $userID)->get();
            $eventsPart = Event::join('Participation', 'Participation.eventID', '=', 'Event.id')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.ID as eventID')->where('ParticipantID', $userID)->where('Accepted', 1)->get();
            $invitation = Participation::join('Event', 'Event.ID', '=', 'Participation.EventID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('Participation.EventID as eventID', 'ParticipantID', 'Title', 'DateOfEvent', 'FullName', 'OrganizerID')->where('Participation.ParticipantID', $userID)->where('Participation.Accepted', null)->distinct('[Participation.EventID]')->get();
            return view('welcome', compact('email', 'password', 'userID', 'userFullNameSession', 'eventsOrg', 'eventsPart', 'invitation', 'userActive', 'gifts')); //compact equivalent viewbag
        }
    }

    public function inscription()
    {
        return view('connexion.register');
    }

    public function register(Request $request)
    {
        $fullname = $request->fullname;
        $email = $request->email;
        $password = $request->password;

        $user = Utilisateur::where('Email', $email)->first();

        if ($user != null) {
            return view('connexion.register');
        } else {

            Utilisateur::create([
                'FullName' => $fullname,
                'Email' => $email,
                'Password' => $password
            ]);

            return view('connexion.login');
        }
    }

    public function Logout()
    {
        session()->remove("userID");
        session()->remove("userFullname");
        return view("connexion.login");
    }
}

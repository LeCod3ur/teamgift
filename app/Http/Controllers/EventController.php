<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Gift;
use App\Models\Participation;
use App\Models\Utilisateur;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class EventController extends Controller
{
    /* public function index() {
        $events = Event::all(); // Equivaut à SELECT * FROM events. Aprèes :: on choisit sa méthode
        return view('events' , compact('events')); // On passe la variable events à la vue events.blade.php
    } */

    public function welcome(Request $request){
        $userID =session()->get("userID");
        $userFullNameSession = session()->get("userFullname");
        $userActive = Utilisateur::all()->where('ID', $userID);
        $gifts = Gift::all();
        $eventsOrg = Event::join('User', 'User.ID', '=' , 'Event.OrganizerID')->join('Gift', 'Gift.ID', '=', 'GiftID')->select('*', 'Gift.Title as giftTitle', 'Event.Title as title', 'Event.ID as eventID' )->where('OrganizerID', $userID)->get();
        $eventsPart = Event::join('Participation', 'Participation.eventID', '=', 'Event.id')->join('User', 'User.ID', '=' , 'Event.OrganizerID')->join('Gift', 'Gift.ID', '=', 'GiftID')->select('*', 'Event.ID as eventID', 'Gift.Title as giftTitle')->where('ParticipantID', $userID)->where('Accepted', 1)->get();
        $invitation = Participation::join('Event', 'Event.ID', '=', 'Participation.EventID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('Participation.EventID as eventID', 'ParticipantID', 'Title', 'DateOfEvent', 'FullName', 'OrganizerID')->where('Participation.ParticipantID', $userID)->where('Participation.Accepted', null)->distinct('[Participation.EventID]')->get();
        return view('welcome', compact('userFullNameSession', 'eventsOrg', 'eventsPart', 'invitation', 'userActive', 'gifts', 'userID'));
    }

    public function acceptInvitation(Request $request){
        $eventID = $request->eventID;
        $participantID = $request->participantID;
        $isAccepted = $request->accepted;

        Participation::where('EventID', $eventID)->where('ParticipantID', $participantID)->update(['Accepted' => $isAccepted]);

        $userID =session()->get("userID");
        $userFullNameSession = session()->get("userFullname");
        $userActive = Utilisateur::all()->where('ID', $userID);
        $gifts = Gift::all();
        $eventsOrg = Event::join('User', 'User.ID', '=' , 'Event.OrganizerID')->join('Gift', 'Gift.ID', '=', 'GiftID')->select('*', 'Gift.Title as giftTitle', 'Event.Title as title', 'Event.ID as eventID' )->where('OrganizerID', $userID)->get();
        $eventsPart = Event::join('Participation', 'Participation.eventID', '=', 'Event.id')->join('User', 'User.ID', '=' , 'Event.OrganizerID')->select('*', 'Event.ID as eventID')->where('ParticipantID', $userID)->where('Accepted', 1)->get();
        $invitation = Participation::join('Event', 'Event.ID', '=', 'Participation.EventID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('Participation.EventID as eventID', 'ParticipantID', 'Title', 'DateOfEvent', 'FullName', 'OrganizerID')->where('Participation.ParticipantID', $userID)->where('Participation.Accepted', null)->distinct('[Participation.EventID]')->get();
        return view('welcome', compact('userFullNameSession', 'eventsOrg', 'eventsPart', 'invitation', 'userActive', 'gifts', 'userID'));
    }

    public function eventDetail(Request $request)
    {
        $ID = $request->ID;
        $userID =session()->get("userID");
        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $ID)->get();
        $participantAccepter = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.FullName')->where('Participation.Accepted', 1)->where('Participation.EventID', $ID)->get();
        $touteContributionParEventID = Participation::where('EventID', $ID)->sum('Contribution');
        $montantCollecte =number_format($touteContributionParEventID, 2);
        return view('events', compact('participantAccepter', 'eventDetail', 'ID', 'montantCollecte', 'userID'));
    }


    public function faireUnDon(Request $request){
        $ID = $request->eventID;
        $userID = $request->participantID;
        $contribution = $request->contribution;
        $ContributionParEventIDParParticipant = Participation::where('EventID', $ID)->where('ParticipantID', $userID)->value('Contribution');

        if($ContributionParEventIDParParticipant == null){
            Participation::where('EventID', $ID)->where('ParticipantID', $userID)->update(['Contribution' => $contribution]);
        } else {
            $nouvelleSomme = $ContributionParEventIDParParticipant + $contribution;
            Participation::where('EventID', $ID)->where('ParticipantID', $userID)->update(['Contribution' => $nouvelleSomme]);
        }


        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $ID)->get();
        $participantAccepter = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.FullName')->where('Participation.Accepted', 1)->where('Participation.EventID', $ID)->get();
        $touteContributionParEventID = Participation::where('EventID', $ID)->sum('Contribution');
        $montantCollecte =number_format($touteContributionParEventID, 2);
        return view('events', compact('participantAccepter', 'eventDetail', 'ID', 'montantCollecte', 'userID'));
    }


    public function addParticipantView(Request $request)
    {
        $ID = $request->ID;
        $userID =session()->get("userID");
        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $ID)->get();
        $participantInvite = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.ID', 'User.FullName')->where('Participation.EventID', $ID)->distinct('[User.ID]')->get();
        $participantNoInvite = Utilisateur::select('*')->whereraw('ID not in (select ParticipantID from Participation where eventid = ?)', [$ID])->get();
        return view('addparticipantView', compact('ID', 'participantInvite', 'participantNoInvite', 'eventDetail', 'userID'));
    }

    public function addParticipant(Request $request){
        $ParticipantID = $request->participantID;
        $EventID = $request->eventID;

        Participation::create([
            'ID',
            'EventID' => $EventID,
            'ParticipantID' => $ParticipantID,
            'Accepted' => null,
            'Contribution' => null
        ]);

        $ID = $request->eventID;
        $userID =$ParticipantID;
        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $ID)->get();
        $participantInvite = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.ID', 'User.FullName')->where('Participation.EventID', $EventID)->distinct('[User.ID]')->get();
        $participantNoInvite = Utilisateur::select('*')->whereraw('ID not in (select ParticipantID from Participation where eventid = ?)', [$EventID])->get();
        return view('addparticipantView', compact('ID', 'participantInvite', 'participantNoInvite', 'ParticipantID', 'eventDetail', 'userID'));

    }

    /* public function invitationForm(Request $request){
        $eventID = $request->eventID;
        $participantID = $request->participantID;
        //$isAccepted = $request->accepted;

        //Participation::where('EventID', $eventID)->where('ParticipantID', $participantID)->update(['Accepted' => $isAccepted]);
        $ID = $request->eventID;
        $userID =session()->get("userID");
        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $eventID)->get();
        $participantInvite = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.ID', 'User.FullName')->where('Participation.EventID', $eventID)->distinct('[User.ID]')->get();
        $participantNoInvite = Utilisateur::select('*')->whereraw('ID not in (select ParticipantID from Participation where eventid = ?)', [$eventID])->get();
        return view('addparticipantView', compact('eventID', 'participantInvite', 'participantNoInvite', 'eventDetail', 'ID'));
    } */

    public function createEventView()
    {
        $userID =session()->get("userID");
        $gifts = Gift::all();
        $partipant = Utilisateur::all();
        return view('createvent', compact('gifts', 'userID', 'partipant'));
    }

    public function createEvent(Request $request){
        $ID = $request->ID;
        $title = $request->title;
        $date = $request->dateOfEvent;
        $organizerID = $request->OrganizerID;

        $dateOfEvent = date("Y-m-d", strtotime($date));
        $giftID = $request->giftID;

        $userID =session()->get("userID");

        Event::create([

            'Title' => $title,
            'DateOfEvent' => $dateOfEvent,
            'OrganizerID' => $organizerID,
            'GiftID' => $giftID
        ]);

        $userID =session()->get("userID");
        $touteContributionParEventID = Participation::where('EventID', $ID)->sum('Contribution');
        $montantCollecte =number_format($touteContributionParEventID, 2);

        $ID = Event::max('ID');
        $eventDetail = Event::join('Gift', 'Gift.ID', '=', 'Event.GiftID')->join('User', 'User.ID', '=', 'Event.OrganizerID')->select('*', 'Event.Title as eventTitle', 'Gift.Title as giftTitle', 'Event.ID as eventID')->where('Event.ID', $ID)->get();
        $participantAccepter = Utilisateur::join('Participation', 'Participation.ParticipantID', '=', 'User.ID')->select('User.FullName')->where('Participation.Accepted', 1)->where('Participation.EventID', $ID)->get();
        return view('events', compact('participantAccepter', 'eventDetail', 'ID', 'montantCollecte', 'userID'));

    }


}


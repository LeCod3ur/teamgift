<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    use HasFactory;
    protected $table = 'User';
    public $timestamps = false;
    protected $primaryKey = 'ID';
    public $incrementing = true;
    protected $fillable = ['ID', 'FullName', 'Email', 'Password'];
}
